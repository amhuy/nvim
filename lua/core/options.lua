local global = require 'core.global'
local vim = global.vim

local bind_option = function(options)
  for k, v in pairs(options) do
    if v == true or v == false then
      vim.cmd('set ' .. k)
    else
      vim.cmd('set ' .. k .. '=' .. v)
    end
  end
end

local load_options = function()
  local global_local = {
    directory = global.cache_dir .. 'swag/',
    undodir = global.cache_dir .. 'undo/',
    backupdir = global.cache_dir .. 'backup/',
    viewdir = global.cache_dir .. 'view/',
    -- spelldir = global.cache_dir .. 'spell/',
    termguicolors = true,
    showtabline = 2, -- always show tab line
    timeoutlen = 1000,
    backspace = 'indent,eol,start', -- make backspace key work right
    clipboard = 'unnamedplus',
    wildignorecase = true,
    wildignore = '.git,.hg,.svn,*.pyc,*.o,*.out,*.jpg,*.jpeg,*.png,*.gif,*.zip,**/tmp/**,*.DS_Store,**/node_modules/**,**/bower_modules/**',
    ignorecase = true,
    smartcase = true,
    infercase = true,
    incsearch = true,
    encoding = 'utf-8',
    fileencodings = 'utf-8,sjis,euc-jp',
  }

  local bw_local = {
    undofile = true, -- undo even if reopen file
    shiftwidth = 4, -- tab space width
    tabstop = 4,
    softtabstop = -1,
    expandtab = true, -- tab to space
    number = true, -- show line number
  }

  for name, value in pairs(global_local) do
    vim.o[name] = value
  end
  bind_option(bw_local)
end

load_options()
