local global = require 'core.global'
local vim = global.vim

local ecmd = function(cmd)
  return '<cmd>' .. cmd .. '<cr>'
end

_G.lint_progress = function()
  local linters = require('lint').get_running()
  if #linters == 0 then
    print '󰦕'
    return '󰦕'
  end
  print('󱉶 ' .. table.concat(linters, ', '))
  return '󱉶 ' .. table.concat(linters, ', ')
end

-- local fmt = 'lua vim.lsp.buf.format();'
local ff = 'Telescope find_files find_command=rg,--hidden,--files,--glob,!.git/*'

_G.grep_dir = function(search_pattern, search_dir)
  if search_pattern == '' then
    return ''
  end
  if search_dir == '' then
    search_dir = '.'
  end
  if string.sub(search_dir, -string.len '/') ~= '/' then
    search_dir = search_dir .. '/'
  end

  local success = pcall(vim.cmd, [[vimgrep /]] .. search_pattern .. [[/gj ]] .. search_dir .. [[**/* ]])
  if success then
    print('Search result [' .. search_pattern .. '] in ' .. search_dir)
    vim.cmd [[copen]]
    vim.cmd([[/]] .. search_pattern)
  else
    print('No match [' .. search_pattern .. '] in ' .. search_dir)
  end
end

local key_map = {
  {
    prefix = '',
    mode = 'n',
    map = {
      lg = { ecmd 'lua lazygit_toggle()', 'LazyGit' },
      mm = { ecmd 'execute "normal <Plug>(comment_toggle_linewise_current)"', 'Toggle comment' },
      mn = { ecmd 'execute "normal <Plug>(comment_toggle_blockwise_current)"', 'Toggle comment' },
      ['\\'] = { ecmd 'Neotree toggle', 'Neotree' },
      ['<A-w>'] = { '<C-w>w', 'Switch window' },
      ['<A-q>'] = { ecmd 'q!', 'Force quit' },
      ['<A-e>'] = { ecmd 'Neotree toggle', 'Neotree' },
      ['<A-m>'] = { ecmd 'Mason', 'Mason' },
      ['<A-l>'] = { ecmd 'Lazy', 'Lazy' },
      ['<A-f>'] = { ecmd(ff), 'Find files' },
      ['<A-g>'] = { ecmd 'Telescope grep_string', 'Grep string in files' },
      ['<A-p>'] = { ecmd 'Telescope projects', 'Recent projects' },
      ['<A-c>'] = { 'ggvGY', 'Yank all' },
      ['<A-t>'] = { ecmd('tabnew|' .. ff), 'New tab' },
      ['<A-1>'] = { '1gt', 'Switch to 1st tab' },
      ['<A-2>'] = { '2gt', 'Switch to 2nd tab' },
      ['<A-3>'] = { '3gt', 'Switch to 3rd tab' },
      ['<A-4>'] = { '4gt', 'Switch to 4th tab' },
      ['<A-5>'] = { '5gt', 'Switch to 5th tab' },
      ['<A-6>'] = { '6gt', 'Switch to 6th tab' },
      ['<A-7>'] = { '7gt', 'Switch to 7th tab' },
      ['<A-8>'] = { '8gt', 'Switch to 8th tab' },
      ['<A-9>'] = { '9gt', 'Switch to 9th tab' },
      ['<A-->'] = { 'gT', 'Switch to previous tab' },
      ['<A-+>'] = { 'gt', 'Switch to next tab' },
      ['<A-d>'] = { ecmd 'Trouble diagnostics', 'Toogle document trouble list' },
      -- ['<A-D>'] = { ecmd 'TroubleToggle workspace_diagnostics', 'Toogle workspace trouble list' },
      ['<A-F>'] = { ecmd 'Trouble quickfix', 'Trouble quick fix' },
      ['<A-B>'] = { ecmd 'lua vim.diagnostic.open_float()', 'Current line diagnostic' },
      g = { R = { ecmd 'TroubleToggle lsp_references', 'Trouble LSP references' } },
      ['<C-a>'] = { 'gg0vG$', 'Select all' },
      ['<C-s>'] = { ecmd 'write', 'Save' },
      -- ['<C-S-f>'] = { ecmd(fmt), 'Format' },
    },
  },
  {
    prefix = '',
    mode = 'i',
    map = {
      jk = { '<Esc>', 'Esc' },
    },
  },
  {
    prefix = '',
    mode = 'v',
    map = {
      m = { ecmd 'execute "normal <Plug>(comment_toggle_linewise_visual)"', 'Toggle comment' },
      n = { ecmd 'execute "normal <Plug>(comment_toggle_blockwise_visual)"', 'Toggle comment' },
      ['<Tab>'] = { '>gv', 'Tab' },
      ['<S-Tab>'] = { '<gv', 'Tab' },
      ['<C-d>'] = { '"_d', 'Delete without yank' },
      ['<C-p>'] = { '"_dP', 'Paste without yank' },
      ['<C-i>'] = { '"_da', 'Replace without yank' },
    },
  },
  {
    prefix = '<leader>',
    mode = 'n',
    map = {
      cs = { ecmd 'set et | ret', 'Convert indent to space' },
      ct = { ecmd 'set noet | ret!', 'Convert indent to tab' },
      fa = { ecmd 'Telescope live_grep', 'Live grep' },
      ff = { ecmd(ff), 'Find files' },
      fg = { ecmd 'Telescope grep_string', 'Grep string in files' },
      S = { ecmd 'SearchBoxIncSearch', 'SearchBox search' },
      R = { ecmd 'SearchBoxReplace confirm=menu', 'SearchBox replace' },
      G = { ecmd 'call v:lua.grep_dir(input("Search pattern: "), input("Search directory: "))', 'Grep directory' },
      sr = { ecmd 'lua require \'ssr\'.open()', 'Structural search and replace' },
      u = { ecmd 'e ++enc=uft8', 'Change encode to UTF-8' },
      j = { ecmd 'e ++enc=sjis', 'Change encode to Shift JIS' },
      i = { ecmd 'e ++enc=euc-jp', 'Change encode to EUC-JP' },
      t = { ecmd 'ToggleTerm direction=horizontal', 'Terminal Horizontal' },
      T = { ecmd 'ToggleTerm direction=vertical', 'Terminal Vertical' },
      y = { ecmd 'echo @% | put =expand(\'%:t\')' .. '0y$u' .. ecmd 'echo "File name yanked."', 'Yank file name' },
      Y = { ecmd 'echo @% | put =@%' .. '0y$u' .. ecmd 'echo "File name yanked."', 'Yank file path' },
      ['?'] = {
        function()
          require('which-key').show { global = false }
        end,
        desc = 'Buffer Local Keymaps (which-key)',
      },
    },
  },
}

return key_map
