local global = require 'core.global'
local vim = global.vim

local create_dir = function()
  local data_dir = {
    global.cache_dir .. 'backup',
    global.cache_dir .. 'session',
    global.cache_dir .. 'swap',
    global.cache_dir .. 'tags',
    global.cache_dir .. 'undo',
  }
  if vim.fn.isdirectory(global.cache_dir) == 0 then
    os.execute('mkdir -p ' .. global.cache_dir)
    for _, v in pairs(data_dir) do
      if vim.fn.isdirectory(v) == 0 then
        os.execute('mkdir -p ' .. v)
      end
    end
  end
end

local load_core = function()
  create_dir()
  require 'core.options'
  require 'plugin'
  require 'core.event'
end

-- vim.api.nvim_create_autocmd('WinLeave', {
--   pattern = '*',
--   callback = function(args)
--     vim.pretty_print(args)
--   end,
-- })

_G.showCursor = function()
  vim.cmd [[set guicursor+=n:ver1-Cursor/lCursor-blinkwait300-blinkon200-blinkoff150]]
end

vim.api.nvim_create_user_command('Cur', 'lua _G.showCursor()', {})

load_core()
