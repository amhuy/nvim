local global = require 'core.global'
local vim = global.vim

local lsps = {}
local lspconf_done = false

local setup_lspconfig = function()
  local cmplsp = require 'cmp_nvim_lsp'
  if not lspconf_done == false or next(lsps) == nil or cmplsp == nil then
    return
  end
  lspconf_done = true
  -- Set up lspconfig.
  local capabilities = cmplsp.default_capabilities()
  -- Replace <YOUR_LSP_SERVER> with each lsp server you've enabled.
  local lspcfg = require 'lspconfig'

  for _, v in pairs(lsps) do
    local settings = { capabilities = capabilities }
    if type(v) == 'table' then
      for k, c in pairs(v[2]) do
        settings[k] = c
      end
      v = v[1]
    end
    lspcfg[v].setup(settings)
  end
end

local file_exists = function(name)
  local f = io.open(name, 'r')
  if f ~= nil then
    io.close(f)
    return true
  else
    return false
  end
end

local plugins = {
  {
    -- =======================================
    'folke/tokyonight.nvim',
    priority = 1000, -- make sure to load this before all the other start plugins
    config = function()
      vim.cmd [[colorscheme tokyonight-night]]
    end,
  },
  {
    -- =======================================
    'nvim-neo-tree/neo-tree.nvim',
    branch = 'v3.x',
    cmd = { 'Neotree' },
    dependencies = {
      'nvim-lua/plenary.nvim',
      'nvim-tree/nvim-web-devicons', -- not strictly required, but recommended
      'MunifTanjim/nui.nvim',
    },
    config = function()
      require('neo-tree').setup {
        close_if_last_window = true, -- Close Neo-tree if it is the last window left in the tableplugs
        mappings = { ['<cr>'] = 'open_drop' },
        filesystem = {
          follow_current_file = {
            enabled = true,
          },
        },
        buffers = {
          follow_current_file = {
            enabled = true,
          },
        },
      }
    end,
  },
  {
    -- =======================================
    'folke/which-key.nvim',
    event = 'VeryLazy',
    dependencies = {
      'echasnovski/mini.icons',
    },
    config = function()
      vim.o.timeout = true
      vim.o.timeoutlen = 300
      local wk = require 'which-key'
      wk.setup {}

      local keymap = require 'core.keymap'
      local arr = {}
      for _, value in pairs(keymap) do
        for k, v in pairs(value.map) do
          table.insert(arr, { value.prefix .. k, v[1], desc = v[2], mode = value.mode })
        end
      end
      wk.add(arr)
    end,
  },
  {
    -- =======================================
    'nvim-lualine/lualine.nvim',
    event = 'BufReadPre',
    dependencies = { 'nvim-tree/nvim-web-devicons' },
    config = function()
      require('lualine').setup {
        theme = 'tokyonight',
        options = {
          icons_enabled = true,
          theme = 'auto',
          component_separators = { left = '', right = '' },
          section_separators = { left = '', right = '' },
          disabled_filetypes = {},
          always_divide_middle = true,
        },
        sections = {
          lualine_a = { 'mode' },
          lualine_b = { 'branch', 'diff', 'diagnostics' },
          lualine_c = { 'filename' },
          lualine_j = { 'mode' },
          lualine_x = { 'encoding', 'fileformat', 'filetype' },
          lualine_y = { 'progress' },
          lualine_z = { 'location' },
        },
        inactive_sections = {
          lualine_a = {},
          lualine_b = {},
          lualine_c = { 'filename' },
          lualine_x = { 'location' },
          lualine_y = {},
          lualine_z = {},
        },
        tabline = {},
        extensions = {},
      }
    end,
  },
  {
    -- =======================================
    'williamboman/mason-lspconfig.nvim',
    event = 'BufReadPre',
    cmd = { 'Mason' },
    dependencies = { 'williamboman/mason.nvim', 'neovim/nvim-lspconfig' },
    config = function()
      lsps = {
        'lua_ls',
        'bashls',
        'perlnavigator',
        'docker_compose_language_service',
        {
          'yamlls',
          {
            settings = {
              yaml = {
                keyOrdering = false,
              },
            },
          },
        },
        'solargraph',
        'jdtls',
        'ts_ls',
        'intelephense',
        'pyright',
      }
      require('mason').setup {}
      -- require('mason-lspconfig').setup {
      --   ensure_installed = lsps,
      -- }
      -- local lsp = require 'lspconfig'
      -- lsp.lua_ls.setup {}
      -- lsp.perlnavigator.setup {}
      local mr = require 'mason-registry'
      local ensure_installed = {
        'lua-language-server',
        'selene',
        'stylua',
        'bash-language-server',
        'shfmt',
        'docker-compose-language-service',
        'yaml-language-server',
        'yamllint',
        'prettierd',
        'typescript-language-server',
        'semgrep',
        'pyright',
        'mypy',
        'pyink',
      }
      for _, v in pairs(ensure_installed) do
        local pkg = mr.get_package(v)
        if pkg:is_installed() == false then
          pkg:install()
        end
      end

      setup_lspconfig()
    end,
  },
  {
    -- =======================================
    -- 'jose-elias-alvarez/null-ls.nvim',
    'nvimtools/none-ls.nvim',
    event = 'BufReadPre',
    dependencies = { 'nvim-lua/plenary.nvim', 'folke/which-key.nvim' },
    config = function()
      local null_ls = require 'null-ls'
      null_ls.setup {
        sources = {
          null_ls.builtins.formatting.prettierd.with {
            extra_args = {
              '--trailing-comma=es5',
              '--tab-width=2',
              '--semi',
              '--single-quote',
            },
          },
          null_ls.builtins.formatting.shfmt.with {
            extra_args = {
              '-i',
              '4',
            },
          },
          null_ls.builtins.diagnostics.yamllint,
          null_ls.builtins.diagnostics.semgrep.with {
            extra_args = {
              '--config',
              'auto',
            },
          },
          -- lua
          null_ls.builtins.diagnostics.selene,
          null_ls.builtins.formatting.stylua.with {
            extra_args = {
              file_exists(global.home .. '/.stylua.toml') and ('--config-path=' .. global.home .. '/.stylua.toml')
                or '',
            },
          },
          -- ruby
          null_ls.builtins.diagnostics.rubocop,
          null_ls.builtins.formatting.rubocop,
          -- java
          null_ls.builtins.formatting.google_java_format,
          -- python
          null_ls.builtins.diagnostics.mypy,
          null_ls.builtins.formatting.pyink,

          null_ls.builtins.completion.tags,
          -- null_ls.builtins.completion.spell,
        },
        on_attach = function(client, bufnr)
          local fmt = function(is_save)
            vim.lsp.buf.format()
            if is_save then
              vim.cmd 'write'
            end
          end

          local wk = require 'which-key'
          wk.add {
            {
              '<A-s>',
              function()
                fmt(true)
              end,
              desc = 'Format and save',
              mode = 'n',
            },
            {
              '<leader>m',
              function()
                fmt(false)
              end,
              desc = 'Format',
              mode = 'n',
            },
          }
        end,
      }
    end,
  },
  {
    -- =======================================
    'nvim-telescope/telescope.nvim',
    cmd = { 'Telescope' },
    branch = '0.1.x',
    dependencies = { 'nvim-lua/plenary.nvim' },
    config = function()
      require('telescope').setup {
        defaults = {
          file_ignore_patterns = { 'node_modules' },
        },
      }
    end,
  },
  {
    -- =======================================
    'hrsh7th/nvim-cmp',
    event = 'BufReadPre',
    dependencies = {
      'neovim/nvim-lspconfig',
      'hrsh7th/cmp-nvim-lsp',
      'hrsh7th/cmp-buffer',
      'hrsh7th/cmp-path',
      'hrsh7th/cmp-cmdline',
      'saadparwaiz1/cmp_luasnip',
    },
    config = function()
      local cmp = require 'cmp'
      cmp.setup {
        snippet = {
          -- REQUIRED - you must specify a snippet engine
          expand = function(args)
            -- vim.fn["vsnip#anonymous"](args.body) -- For `vsnip` users.
            require('luasnip').lsp_expand(args.body) -- For `luasnip` users.
            -- require('snippy').expand_snippet(args.body) -- For `snippy` users.
            -- vim.fn["UltiSnips#Anon"](args.body) -- For `ultisnips` users.
          end,
        },
        window = {
          -- completion = cmp.config.window.bordered(),
          -- documentation = cmp.config.window.bordered(),
        },
        mapping = cmp.mapping.preset.insert {
          ['<C-b>'] = cmp.mapping.scroll_docs(-4),
          ['<C-f>'] = cmp.mapping.scroll_docs(4),
          ['<C-Space>'] = cmp.mapping.complete(),
          ['<C-e>'] = cmp.mapping.abort(),
          ['<CR>'] = cmp.mapping.confirm { select = true },
          -- Accept currently selected item. Set `select` to `false` to only confirm explicitly selected items.
        },
        sources = cmp.config.sources({
          { name = 'nvim_lsp' },
          -- { name = 'vsnip' }, -- For vsnip users.
          { name = 'luasnip' }, -- For luasnip users.
          -- { name = 'ultisnips' }, -- For ultisnips users.
          -- { name = 'snippy' }, -- For snippy users.
        }, { { name = 'buffer' } }),
      }

      -- Set configuration for specific filetype.
      cmp.setup.filetype('gitcommit', {
        sources = cmp.config.sources({
          { name = 'cmp_git' }, -- You can specify the `cmp_git` source if you were installed it.
        }, { { name = 'buffer' } }),
      })

      -- Use buffer source for `/` and `?` (if you enabled `native_menu`, this won't work anymore).
      cmp.setup.cmdline({ '/', '?' }, {
        mapping = cmp.mapping.preset.cmdline(),
        sources = { { name = 'buffer' } },
      })

      -- Use cmdline & path source for ':' (if you enabled `native_menu`, this won't work anymore).
      cmp.setup.cmdline(':', {
        mapping = cmp.mapping.preset.cmdline(),
        sources = cmp.config.sources({ { name = 'path' } }, { { name = 'cmdline' } }),
      })

      setup_lspconfig()
    end,
  },
  {
    -- =======================================
    'L3MON4D3/LuaSnip',
    event = 'BufReadPre',
    -- follow latest release.
    -- version = '<CurrentMajor>.*',
    -- version = 'v1.2.*',
    -- install jsregexp (optional!).
    -- build = 'make install_jsregexp',
    dependencies = {
      'honza/vim-snippets',
    },
    config = function()
      local ls = require 'luasnip'
      ls.setup {}
      ls.filetype_extend('all', { '_' })
      require('luasnip.loaders.from_snipmate').lazy_load {}
    end,
  },
  {
    -- =======================================
    'JoosepAlviste/nvim-ts-context-commentstring',
    event = 'BufReadPre',
    dependencies = {
      {
        'nvim-treesitter/nvim-treesitter',
        build = ':TSUpdate',
        event = { 'BufReadPost', 'BufNewFile' },
        config = function()
          vim.g.skip_ts_context_commentstring_module = true
          require('ts_context_commentstring').setup {}
          require('nvim-treesitter.configs').setup {
            ensure_installed = {
              -- 'help',
              'lua',
              'javascript',
              'json',
              'html',
              'scss',
              'css',
              'java',
              'php',
              'ruby',
              'python',
              'perl',
              'yaml',
              'bash',
              'dockerfile',
              'tsx',
              'markdown',
              'typescript',
            },
            context_commentstring = {
              enable = true,
              enable_autocmd = false,
              config = {
                css = '/* %s */',
                scss = { __default = '// %s', __multiline = '/* %s */' },
                php = { __default = '// %s', __multiline = '/* %s */' },
                html = '<!-- %s -->',
                tsx = {
                  __default = '// %s',
                  __multiline = '/* %s */',
                  jsx_element = '{/* %s */}',
                  jsx_fragment = '{/* %s */}',
                  jsx_attribute = { __default = '// %s', __multiline = '/* %s */' },
                  comment = { __default = '// %s', __multiline = '/* %s */' },
                  call_expression = { __default = '// %s', __multiline = '/* %s */' },
                  statement_block = { __default = '// %s', __multiline = '/* %s */' },
                  spread_element = { __default = '// %s', __multiline = '/* %s */' },
                },
                javascript = {
                  __default = '// %s',
                  __multiline = '/* %s */',
                  jsx_element = '{/* %s */}',
                  jsx_fragment = '{/* %s */}',
                  jsx_attribute = { __default = '// %s', __multiline = '/* %s */' },
                  comment = { __default = '// %s', __multiline = '/* %s */' },
                  call_expression = { __default = '// %s', __multiline = '/* %s */' },
                  statement_block = { __default = '// %s', __multiline = '/* %s */' },
                  spread_element = { __default = '// %s', __multiline = '/* %s */' },
                },
              },
            },
          }
        end,
      },
    },
  },
  {
    -- =======================================
    'glepnir/dashboard-nvim',
    event = 'VimEnter',
    config = function()
      require('dashboard').setup {
        theme = 'hyper',
        config = {
          week_header = {
            enable = true,
          },
          shortcut = {
            { desc = ' Update', group = '@property', action = 'Lazy update', key = 'u' },
            {
              icon = ' ',
              icon_hl = '@variable',
              desc = 'Files',
              group = 'Label',
              action = 'Telescope find_files',
              key = 'f',
            },
          },
        },
        hide = {
          tabline = false, -- hide the tabline
        },
      }
    end,
    dependencies = { 'nvim-tree/nvim-web-devicons' },
  },
  {
    -- =======================================
    'numToStr/Comment.nvim',
    event = 'BufReadPre',
    config = function()
      require('Comment').setup {
        pre_hook = require('ts_context_commentstring.integrations.comment_nvim').create_pre_hook(),
      }
    end,
  },
  {
    -- =======================================
    'windwp/nvim-autopairs',
    event = 'BufReadPre',
    config = function()
      require('nvim-autopairs').setup {}
    end,
  },
  {
    -- =======================================
    'kylechui/nvim-surround',
    event = 'BufReadPre',
    -- version = '*', -- Use for stability; omit to use `main` branch for the latest features
    config = function()
      require('nvim-surround').setup {
        -- Configuration here, or leave empty to use defaults
      }
    end,
  },
  {
    -- =======================================
    'lukas-reineke/indent-blankline.nvim',
    event = 'BufReadPre',
    main = 'ibl',
    opts = {},
    config = function()
      local highlight = {
        'CursorColumn',
        'Whitespace',
        -- 'RainbowRed',
        -- 'RainbowYellow',
        -- 'RainbowBlue',
        -- 'RainbowOrange',
        -- 'RainbowGreen',
        -- 'RainbowViolet',
        -- 'RainbowCyan',
      }
      -- local highlight2 = {
      --   'CursorColumn',
      --   'Whitespace',
      -- }

      -- local hooks = require 'ibl.hooks'
      -- hooks.register(hooks.type.HIGHLIGHT_SETUP, function()
      --   vim.api.nvim_set_hl(0, 'RainbowRed', { fg = '#E06C75' })
      --   vim.api.nvim_set_hl(0, 'RainbowYellow', { fg = '#E5C07B' })
      --   vim.api.nvim_set_hl(0, 'RainbowBlue', { fg = '#61AFEF' })
      --   vim.api.nvim_set_hl(0, 'RainbowOrange', { fg = '#D19A66' })
      --   vim.api.nvim_set_hl(0, 'RainbowGreen', { fg = '#98C379' })
      --   vim.api.nvim_set_hl(0, 'RainbowViolet', { fg = '#C678DD' })
      --   vim.api.nvim_set_hl(0, 'RainbowCyan', { fg = '#56B6C2' })
      -- end)

      vim.opt.list = true
      vim.opt.listchars:append 'space:⋅'
      vim.opt.listchars:append 'eol:↴'
      -- vim.g.rainbow_delimiters = { highlight = highlight }

      require('ibl').setup {
        indent = { highlight = highlight, char = '' },
        whitespace = {
          highlight = highlight,
          remove_blankline_trail = false,
        },
        scope = { enabled = false },

        -- scope = { highlight = highlight },
      }
      -- hooks.register(hooks.type.SCOPE_HIGHLIGHT, hooks.builtin.scope_highlight_from_extmark)
    end,
  },
  {
    -- =======================================
    'echasnovski/mini.indentscope',
    version = '*',
    config = function()
      require('mini.indentscope').setup {
        draw = {
          delay = 0,
          animation = require('mini.indentscope').gen_animation.none(),
        },
      }
    end,
  },
  {
    -- =======================================
    'cshuaimin/ssr.nvim',
    event = 'BufReadPre',
    -- Calling setup is optional.
    config = function()
      require('ssr').setup {
        min_width = 50,
        min_height = 5,
        max_width = 120,
        max_height = 25,
        keymaps = {
          close = 'q',
          next_match = 'n',
          prev_match = 'N',
          replace_confirm = '<cr>',
          replace_all = '<leader><cr>',
        },
      }
    end,
  },
  {
    -- =======================================
    'lewis6991/gitsigns.nvim',
    event = 'BufReadPre',
    -- version = 'release',
    config = function()
      require('gitsigns').setup {
        current_line_blame = true,
        current_line_blame_opts = {
          delay = 500,
        },
      }
    end,
  },
  {
    -- =======================================
    'akinsho/toggleterm.nvim',
    -- version = '*',
    config = function()
      require('toggleterm').setup {
        size = function(term)
          if term.direction == 'horizontal' then
            return 15
          elseif term.direction == 'vertical' then
            return vim.o.columns * 0.4
          end
        end,
      }
      local Terminal = require('toggleterm.terminal').Terminal
      local lazygit = Terminal:new {
        cmd = 'lazygit',
        dir = 'git_dir',
        direction = 'float',
        float_opts = {
          border = 'double',
        },
        -- function to run on opening the terminal
        on_open = function(term)
          vim.cmd 'startinsert!'
          vim.api.nvim_buf_set_keymap(term.bufnr, 'n', 'q', '<cmd>close<CR>', { noremap = true, silent = true })
        end,
        -- function to run on closing the terminal
        on_close = function(_)
          vim.cmd 'startinsert!'
        end,
      }

      _G.lazygit_toggle = function()
        lazygit:toggle()
      end

      _G.set_terminal_keymaps = function()
        local opts = { buffer = 0 }
        -- vim.keymap.set('t', '<esc>', [[<C-\><C-n>]], opts)
        vim.keymap.set('t', 'jk', [[<C-\><C-n>]], opts)
        vim.keymap.set('t', '<C-h>', [[<Cmd>wincmd h<CR>]], opts)
        vim.keymap.set('t', '<C-j>', [[<Cmd>wincmd j<CR>]], opts)
        vim.keymap.set('t', '<C-k>', [[<Cmd>wincmd k<CR>]], opts)
        vim.keymap.set('t', '<C-l>', [[<Cmd>wincmd l<CR>]], opts)
        vim.keymap.set('t', '<A-w>', [[<C-\><C-n><C-w>w]], opts)
        vim.keymap.set('t', '<A-q>', [[<C-c><C-d>]], opts)
      end

      vim.cmd 'autocmd! TermOpen term://* lua set_terminal_keymaps()'
    end,
  },
  {
    -- =======================================
    'folke/trouble.nvim',
    event = 'BufReadPre',
    dependencies = 'nvim-tree/nvim-web-devicons',
    config = function()
      require('trouble').setup {}
    end,
  },
  {
    -- =======================================
    'NvChad/nvim-colorizer.lua',
    config = function()
      require('colorizer').setup {}
    end,
  },
  {
    -- =======================================
    'VonHeikemen/searchbox.nvim',
    dependencies = 'MunifTanjim/nui.nvim',
  },
  {
    -- =======================================
    'ahmedkhalf/project.nvim',
    config = function()
      require('project_nvim').setup {}
    end,
  },
}

return plugins
