local plugins = require 'plugin.plugins'
local global = require 'core.global'
local vim = global.vim

local lazypath = vim.fn.stdpath 'data' .. global.path_sep .. 'lazy' .. global.path_sep .. 'lazy.nvim'
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system {
    'git',
    'clone',
    '--filter=blob:none',
    'https://github.com/folke/lazy.nvim.git',
    '--branch=stable', -- latest stable release
    lazypath,
  }
end
vim.opt.rtp:prepend(lazypath)
vim.g.mapleader = ' ' -- make sure to set `mapleader` before lazy so your mappings are correct
require('lazy').setup(plugins)
