# nvim

neovim config

## Getting started

```
mkdir ~/.config
git clone https://gitlab.com/amhuy/nvim.git ~/.config/nvim
```

## Using plugins

-   [Tokyo Night](https://github.com/folke/tokyonight.nvim): theme
-   [Neo-tree.nvim](https://github.com/nvim-neo-tree/neo-tree.nvim): file explorer
-   [Which key](https://github.com/folke/which-key.nvim): shortcut key setting and show suggest
-   [lualine.vim](https://github.com/nvim-lualine/lualine.nvim): status line
-   [mason.nvim](https://github.com/williamboman/mason.nvim): lsp, linters, formatters
-   [null-ls.nvim](https://github.com/jose-elias-alvarez/null-ls.nvim): lsp, linters, formatters, snippet config
-   [telescope.nvim](https://github.com/nvim-telescope/telescope.nvim): useful dialog for searrch,...
-   [nvim-cmp](https://github.com/hrsh7th/nvim-cmp): code completion
-   [LuaSnip](https://github.com/L3MON4D3/LuaSnip): snippet engine
-   [nvim-treesitter](https://github.com/nvim-treesitter/nvim-treesitter): code parser
-   [dashboard-nvim](https://github.com/glepnir/dashboard-nvim): dashboard
-   [Comment.nvim](https://github.com/numToStr/Comment.nvim): code comment
-   [nvim-autopairs](https://github.com/windwp/nvim-autopairs): auto complete pairs
-   [nvim-surround](https://github.com/kylechui/nvim-surround): change surround character,...
-   [Indent Blankline](https://github.com/lukas-reineke/indent-blankline.nvim): add indentation line
-   [ssr.nvim](https://github.com/cshuaimin/ssr.nvim): structural search and replace
-   [gitsigns.nvim](https://github.com/lewis6991/gitsigns.nvim): Git lens like
-   [trouble.nvim](https://github.com/lewis6991/gitsigns.nvim): showing diagnostics, references, telescope results, quickfix and location lists
-   [nvim-colorizer.lua](https://github.com/lewis6991/gitsigns.nvim): color highlighter
-   [toggleterm.nvim](https://github.com/akinsho/toggleterm.nvim): persist and toggle multiple terminals
-   [searchbox.nvim](https://github.com/VonHeikemen/searchbox.nvim): search and replace current file
